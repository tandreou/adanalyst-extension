//The MIT License
//
//Copyright (c) 2018 Athanasios Andreou, <andreou@eurecom.fr>
//
//Permission is hereby granted, free of charge, 
//to any person obtaining a copy of this software and 
//associated documentation files (the "Software"), to 
//deal in the Software without restriction, including 
//without limitation the rights to use, copy, modify, 
//merge, publish, distribute, sublicense, and/or sell 
//copies of the Software, and to permit persons to whom 
//the Software is furnished to do so, 
//subject to the following conditions:
//
//The above copyright notice and this permission notice 
//shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
//EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
//OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
//IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
//ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
//TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
//SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.




const INTERFACE_VERSIONS = {unknown:"UNKNOWN",old:"OLD",new:"NEW"};



function initializeUserInterfaceVersion(userId){
   if (!localStorage.userInterfaceVersion) {

        localStorage.userInterfaceVersion = JSON.stringify({});
    }

    let allVersions = JSON.parse(localStorage.userInterfaceVersion);

    if (!allVersions.hasOwnProperty(userId)) {
        allVersions[userId] = INTERFACE_VERSIONS.unknown;
        localStorage.userInterfaceVersion = JSON.stringify(allVersions);

    }

}


function setUserInterfaceVersion(userId,version) {
	
	initializeUserInterfaceVersion(userId);
    let allVersions = JSON.parse(localStorage.userInterfaceVersion);
    allVersions[userId] = version;
    localStorage.userInterfaceVersion = JSON.stringify(allVersions); 
	
	}

function getUserInterfaceVersion(userId) {

	initializeUserInterfaceVersion(userId);

	return JSON.parse(localStorage.userInterfaceVersion)[userId];
}

function getFacebookInterfaceVersionDoc(resp) {
    let title = resp.match('id="facebook"');
    if (title===null) {
        return INTERFACE_VERSIONS.unknown;
    }

    if (title.length <1) {
            return INTERFACE_VERSIONS.unknown;
    }

	let hasIds = resp.match('userNavigationLabel');

    if (hasIds===null) {
            return INTERFACE_VERSIONS.new;
    }

    if (hasIds.length <1) {
            return INTERFACE_VERSIONS.new;
    }


    return INTERFACE_VERSIONS.old;
}


function setFacebookInterfaceVersionDoc(userId,resp) {
	setUserInterfaceVersion(userId,getFacebookInterfaceVersionDoc(resp));
}






function getFacebookInterfaceVersionFromParsedDoc(doc) {

    let hasTitle = !!doc.getElementById('facebook');
    if (hasTitle===false) {
        return INTERFACE_VERSIONS.unknown;

    }

    return !!doc.getElementById('userNavigationLabel')?INTERFACE_VERSIONS.old:INTERFACE_VERSIONS.new;
}
