# Ad Analyst

AdAnalyst is a browser addon that increases transparency in the Facebook Advertising ecosystem. It analyzes the ads you receive in Facebook and shows you what attributes Facebook has inferred about you, what attributes advertisers use to target you, and much much more.

It works silently in the background while you are browsing Facebook without any overhead to your browsing experience.



Learn more at: https://adanalyst.mpi-sws.org/


## Installation

The project is both available as a [Google Chrome Extension](https://chrome.google.com/webstore/detail/transpad/dichdbdjmcpgniopphedodbhkcdkoglm) and a [Firefox Add-on](https://addons.mozilla.org/en-US/firefox/addon/adanalyst/)

You can install the extension from the source code, by loading it as an unpacked extension, but we recommend that you don't, in order not to miss the auto-updates.



## License

This software is available under the MIT License


